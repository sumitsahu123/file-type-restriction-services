package com.projak.kotak.Document.Scrutinizer.service.impl;

import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.projak.kotak.Document.Scrutinizer.model.OsDocument;
import com.projak.kotak.Document.Scrutinizer.model.OsList;
import com.projak.kotak.Document.Scrutinizer.model.OsMime;
import com.projak.kotak.Document.Scrutinizer.repository.DocRepository;
import com.projak.kotak.Document.Scrutinizer.repository.ListRepository;
import com.projak.kotak.Document.Scrutinizer.repository.MimeRepository;
import com.projak.kotak.Document.Scrutinizer.service.MimeService;

@Service
public class MimeServiceImpl implements MimeService {

	private static final Logger log = LoggerFactory.getLogger(MimeServiceImpl.class);

	/* Instance of AuthenticationManager for LDAP Operations */
	@Autowired
	private AuthenticationManager manager;

	@Autowired
	private ListRepository listRepo;

	@Autowired
	private DocRepository docRepo;

	@Autowired
	private MimeRepository mimeRepo;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MimeServiceImpl.class);

	/*
	 * This Method will be used to create entry in the database.
	 */
	@SuppressWarnings({ "unused", "null" })
	@Override
	public JSONObject create(JSONObject jobj) throws Exception
	{	
				
		LOGGER.info("Inside create method");
		String response= null;
		try
		{
			
		String objectStore=null;
		String documentName = null;
		JSONArray mimeTypeArray = null;		 
		 
		if(jobj.has("OS") && jobj.has("document") && jobj.has("mimeType")) 
		{
			
		LOGGER.info("Os document and mime?type are not null");	
		objectStore = jobj.get("OS").toString();
		documentName = jobj.get("document").toString();
		mimeTypeArray = jobj.getJSONArray("mimeType");
		
		LOGGER.info("Object Store :"+objectStore+"document name :"+documentName+"mimetype :"+mimeTypeArray.length());
			
		if(objectStore!=null && documentName!=null || !documentName.isEmpty() && mimeTypeArray!=null || mimeTypeArray.isEmpty()) 
		{
			OsList osListObj = listRepo.getOsByName(objectStore);
			if(osListObj==null) 
			{
				OsList osList=null;
				OsDocument osDocument=null;
				if(objectStore==null || objectStore.isEmpty()) 
				{
					LOGGER.info("Value of object store is empty");
				}
				else
				{	
				LOGGER.info("Object store is null");
				osList=new OsList();
				osList.setObjectStore(objectStore);
				osList.setStatus(true);
				listRepo.save(osList);	
				LOGGER.info("Data saved in Os List");
				}
				if(documentName==null || documentName.isEmpty()) 
				{
					LOGGER.info("Value of document is empty");
				}
				else
				{
				    osDocument=new OsDocument();
					osDocument.setOsId(osList);
					osDocument.setStatus(true);
					osDocument.setSymbolicName(documentName);
					docRepo.save(osDocument);
					LOGGER.info("Data saved in os Document");
				}
					
					for (int i = 0; i < mimeTypeArray.length(); i++) 
					{
						    JSONObject mimeType = mimeTypeArray.getJSONObject(i);
						    String mimeTypes = mimeType.get("value").toString();
						
							if(mimeType==null || mimeType.isEmpty()) 
							{
								LOGGER.info("Value of mime type is empty");
							}
							else
							{	
							OsMime osMimeObj=new OsMime();
							osMimeObj.setMimeType(mimeTypes);
							osMimeObj.setOsDocument(osDocument);
							mimeRepo.save(osMimeObj);
						    LOGGER.info("Data saved in os mime");
							response="Success";
							}
					}
				}	
				
				
			else 
			{
				OsDocument osDocumentObj = docRepo.findBySymbolicNameAndOsId(documentName,osListObj);
				if(osDocumentObj==null)
				{
					LOGGER.info(" OS Document is null");
					OsDocument osDocument=new OsDocument();
					if(documentName==null || documentName.isEmpty())
					{
						LOGGER.info("Value of document is empty");
					}	
					else
					{	
					osDocument.setOsId(osListObj);
					osDocument.setStatus(true);
					osDocument.setSymbolicName(documentName);
					docRepo.save(osDocument);
					LOGGER.info("Data saved in Os document");
					}
					
					for (int i = 0; i < mimeTypeArray.length(); i++) 
					{
						JSONObject mimeType = mimeTypeArray.getJSONObject(i);
						String mimeTypes = mimeType.get("value").toString();

						    if(mimeType==null || mimeType.isEmpty())
						    {
						    	LOGGER.info("Value of mime type is empty");
						    }
						    else
						    {	
							OsMime osMimeObj=new OsMime();
							osMimeObj.setMimeType(mimeTypes);
							osMimeObj.setOsDocument(osDocument);
							mimeRepo.save(osMimeObj);
							LOGGER.info("Data saved in osmime");
							response="Success";
						    }
					}
				}
				else
				{
					Boolean status=false;
					for (int i = 0; i < mimeTypeArray.length(); i++) 
					{
						JSONObject mimeType = mimeTypeArray.getJSONObject(i);
						String mimeTypes = mimeType.get("value").toString();
						
						OsMime osMime = mimeRepo.findByMimeTypeAndOsDocument(mimeTypes,osDocumentObj);
						if(osMime==null)
						{
							LOGGER.info("Osmime is null");
							if(mimeType==null || mimeType.isEmpty())
							{
								LOGGER.info("Value of mime type is empty");
							}
							OsMime osMimeObj=new OsMime();
							osMimeObj.setMimeType(mimeTypes);
							osMimeObj.setOsDocument(osDocumentObj);
							mimeRepo.save(osMimeObj);
							LOGGER.info("Data saved in osmime");
							status=true;
							response="Success";
						}
						else
						{
							response="Document class is present";
						}
					}
					if(status) 
					{
						response="Success";
					}
				}
				
			}
		 }	
		
		}
		
		else if(jobj.has("OS") && jobj.has("document")) 
		{
			LOGGER.info("OS and document are not null");
			objectStore = jobj.get("OS").toString();
			documentName = jobj.get("document").toString();
			LOGGER.info("object store :"+objectStore+"document :"+documentName);
			
		 if(objectStore!=null  && documentName!=null || !documentName.isEmpty()) 
		    {
			response="values of OS and documentName are empty";			 
        	}
		 
		 else
     		{

				OsList osListObj = listRepo.getOsByName(objectStore);
				if(osListObj==null) 
				{
					OsList osList = null;
					if(objectStore==null || objectStore.isEmpty())
					{
						LOGGER.info("Value of object store is empty");
					}
					else
					{
					LOGGER.info("Os list object is null");
					osList=new OsList();
					osList.setObjectStore(objectStore);
					osList.setStatus(true);
					listRepo.save(osList);	
					LOGGER.info("Data saved in osList");
					}
					if(documentName==null || documentName.isEmpty()) 
					{
						LOGGER.info("Value of document name is empty");
					}
					else
					{	
						OsDocument osDocument=new OsDocument();
						osDocument.setOsId(osList);
						osDocument.setStatus(true);
						osDocument.setSymbolicName(documentName);
						docRepo.save(osDocument);
						LOGGER.info("Data saved on os Document");
						response="Success";	
					}	
				}
				else
				{
					OsDocument osDocumentObj = docRepo.findBySymbolicNameAndOsId(documentName, osListObj);
					if(osDocumentObj==null)
					{
						if(documentName==null || documentName.isEmpty()) 
						{
							LOGGER.info("Value of Document Name is empty");
						}
						else
						{	
						LOGGER.info("os document object is null");
						OsDocument osDocument=new OsDocument();
						osDocument.setOsId(osListObj);
						osDocument.setStatus(true);
						osDocument.setSymbolicName(documentName);
						docRepo.save(osDocument);
						LOGGER.info("Data saved in os document");
	                    response="Success";
						}
					}
					else
					{
						osDocumentObj.setStatus(true);
						docRepo.save(osDocumentObj);
						response="Document class is present";
					}
				}
				
				
			
		 }
	 }	
		
		else if(jobj.has("OS")) 
		{	
			LOGGER.info("Object store is not null");
			objectStore = jobj.get("OS").toString();
			
		 if(objectStore==null || objectStore.isEmpty() ) 
		 {
			 response="value of OS is empty";
		 }	
		 else
		 {
			 
			 OsList osListObj = listRepo.getOsByName(objectStore);
				if(osListObj==null) 
				{
					LOGGER.info("Object store is null");
					OsList osList=new OsList();
					osList.setObjectStore(objectStore);
					osList.setStatus(true);
					listRepo.save(osList);	
					LOGGER.info("Data saved in os List");
					response="Success";
				}
				else
				{
					response="OS is present";
				}
		 }
		}		
		
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		
		JSONObject res=new JSONObject();
		res.put("Message", response);	
		LOGGER.info("Response is :"+res);
		
		return res;
	}

	/*
	 * This Method will be used to delete or disable entry in the database.
	 */
	@Override
	public JSONObject delete(JSONObject jobj) throws Exception 
	{
		LOGGER.info("Inside delete method");
		String response= null;
		try
		{
			
		String objectStore=null;
		String documentName = null;
		JSONArray mimeTypeArray = null;		 
		 
		if(jobj.has("OS") && jobj.has("document") && jobj.has("mimeType")) 
		{
		
		LOGGER.info("OS document and mime type are not null");	
		objectStore = jobj.get("OS").toString();
		documentName = jobj.get("document").toString();
		mimeTypeArray = jobj.getJSONArray("mimeType");
		LOGGER.info("Object Store :"+objectStore+"document name :"+documentName+"mime types :"+mimeTypeArray.length());
			
		if(objectStore!=null && documentName!=null || !documentName.isEmpty() && mimeTypeArray!=null || mimeTypeArray.isEmpty()) 
		{
			OsList osListObj = listRepo.getOsByName(objectStore);
						
			OsDocument osDocument = docRepo.findBySymbolicNameAndOsId(documentName, osListObj);
			
			LOGGER.info("Status updated in osDocument");
			
			for (int i = 0; i < mimeTypeArray.length(); i++) 
			{
				JSONObject mimeType = mimeTypeArray.getJSONObject(i);
				String mimeTypes = mimeType.get("value").toString();
				
				OsMime osMime = mimeRepo.findByMimeTypeAndOsDocument(mimeTypes,osDocument);
				if(osMime!=null) 
				{
				LOGGER.info("Os mime type is not null");	
				mimeRepo.delete(osMime);
				LOGGER.info("Delete mime type is :"+osMime);
				response="Success";
				}
				else 
				{
				LOGGER.info("Document is not present");
				response="Document is not present";
				}
			}
		 }	
		
		}
		
		else if(jobj.has("OS") && jobj.has("document")) 
		{
			LOGGER.info("OS and document are not null");
			objectStore = jobj.get("OS").toString();
			documentName = jobj.get("document").toString();
			LOGGER.info("object store :"+objectStore+"document name :"+documentName);
			
		 if(objectStore!=null  && documentName!=null || !documentName.isEmpty()) 
		  {
			OsList osListObj = listRepo.getOsByName(objectStore);
					
			OsDocument osDocument = docRepo.findBySymbolicNameAndOsId(documentName,osListObj);
			osDocument.setStatus(false);
			docRepo.save(osDocument);
			LOGGER.info("Status updated in os Document");
			
			response="Succcess";
			
		  }
	 }	
		
		else if(jobj.has("OS")) 
		{		
			LOGGER.info("Os is not null");
			objectStore = jobj.get("OS").toString();
			LOGGER.info("Object store is :"+objectStore);
			
		 if(objectStore!=null ) 
		 {
			 
			OsList osListObj = listRepo.getOsByName(objectStore);
			
				osListObj.setStatus(false);
				listRepo.save(osListObj);	
				LOGGER.info("Status updated in OsList");
				response="Success";			
		 }	
		 else
		 {
			  response="Object store is not present";
		 }
		
		}		
		
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		
		JSONObject res=new JSONObject();
		res.put("Message", response);	
		LOGGER.info("Response is :"+response);
		
		return res;
		
	}

	/* This method will be used to check for valid mime types for the document. */
	@Override
	public boolean check(String OS, String document, String mime) {
		OsList os = listRepo.getOsByName(OS);
		if (os != null) {
			log.info("OS "+OS+" is present");
			if (os.isStatus()) {
				System.out.println(os.toString());
				OsDocument doc = docRepo.getDocByName(document, os.getId());
				if (doc != null) {
					log.info("doc "+document+" is present");
					return docMime(doc, mime);
				} else {
					OsDocument documentClass = docRepo.getDocByName("Document", os.getId());
					if (documentClass != null) {
						return docMime(documentClass, mime);
					} else {
						return true;
					}
				}
			} else {
				return true;
			}
		} else {
			return true;
		}

	}

	public boolean docMime(OsDocument doc, String mime) {
		if (doc.isStatus()) {
			OsMime mimeObject = mimeRepo.checkMimeType(doc.getId(), mime);
			if (mimeObject != null) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	/*
	 * This method should be used to authenticate the user and check if user is
	 * present in the @groupName. Returns true, if user is authentic and present
	 * in @groupName else it will return false.
	 * 
	 */
	public boolean authentication(String userName, String password, String groupName) {
		log.info("Inside authentication function");
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userName, password);
		Authentication authenticate = null;
		try {
			authenticate = manager.authenticate(token);
			log.info("user " + userName + " is authenticated");
			boolean isPresent = Optional.ofNullable(authenticate)
					.map(authentication -> authentication.getAuthorities().stream()
							.anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(groupName)))
					.orElse(false);
			if (isPresent) {
				log.info("User " + userName + " is present in group-" + groupName);
			} else {
				log.info("User " + userName + " is present in group-" + groupName);
			}
			return isPresent;
		} catch (Exception ex) {
			log.error("Exception on Authentication of user : " + userName);
			log.error(ex.getMessage());
			ex.fillInStackTrace();
			return false;
		}

	}

}
