package com.projak.kotak.Document.Scrutinizer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "OS_LIST",schema = "hrupm")
public class OsList {
	
	private int id;
	
	private String objectStore;
	
	private boolean status;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "list_sequence")
	@SequenceGenerator(allocationSize = 1,name = "list_sequence",sequenceName = "list_sequence")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "OBJECT_STORE")
	public String getObjectStore() {
		return objectStore;
	}

	public void setObjectStore(String objectStore) {
		this.objectStore = objectStore;
	}

	@Column(name = "STATUS")
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	

	@Override
	public String toString() {
		return "OsList [id=" + id + ", objectStore=" + objectStore + ", status=" + status + "]";
	}

}
