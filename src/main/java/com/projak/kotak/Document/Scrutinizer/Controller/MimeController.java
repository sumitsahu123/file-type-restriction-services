package com.projak.kotak.Document.Scrutinizer.Controller;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projak.kotak.Document.Scrutinizer.service.MimeService;

@RestController
@RequestMapping("/mime")
public class MimeController {

	private static final Logger log = LoggerFactory.getLogger(MimeController.class);

	@Autowired
	private MimeService mime;

	@PostMapping(value = "/check", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> check(@RequestBody String objString) {
		JSONObject jobj = new JSONObject(objString);
		System.out.println(jobj.toString());
		log.info("Calling Check Function");
		if (!jobj.getString("OS").isEmpty()) {
			log.info("OS is present");
			if (!jobj.getString("document").isEmpty()) {
				log.info("document is present");
				if (!jobj.getString("mimeType").isEmpty()) {
					log.info("mimeType is present");
					boolean result = mime.check(jobj.getString("OS"), jobj.getString("document"),
							jobj.getString("mimeType"));
					log.info("Result : "+result);
					return new ResponseEntity<String>(String.valueOf(result), HttpStatus.OK);
				} else {
					log.error("mimeType is absent");
					return new ResponseEntity<String>("mimeType is mandatory and not present", HttpStatus.BAD_REQUEST);
				}
			} else {
				log.error("document is absent");
				return new ResponseEntity<String>("document is mandatory and not present", HttpStatus.BAD_REQUEST);
			}
		} else {
			log.error("OS is absent");
			return new ResponseEntity<String>("OS is mandatory and not present", HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping(value = "/create",consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@RequestBody String jsonObj ) throws Exception 
	{
		JSONObject jobj=new JSONObject(jsonObj);
		JSONObject jsonObject = mime.create(jobj);
		return new ResponseEntity<String>(jsonObject.toString(),HttpStatus.OK);
	}

	@DeleteMapping(value = "/remove",consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> delete(@RequestBody String jsonObj) throws Exception 
	{
		JSONObject jobj=new JSONObject(jsonObj);
		JSONObject jsonObject = mime.delete(jobj);
		return new ResponseEntity<String>(jsonObject.toString(),HttpStatus.OK);
	}

}
