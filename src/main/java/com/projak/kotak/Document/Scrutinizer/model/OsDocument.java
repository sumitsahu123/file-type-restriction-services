package com.projak.kotak.Document.Scrutinizer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "OS_DOCUMENT")
public class OsDocument {
	
	
	private int id;
	
	private OsList osId;
	
	private String symbolicName;
	
	private boolean status;

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "doc_sequence")
	@SequenceGenerator(allocationSize = 1,name = "doc_sequence",sequenceName = "doc_sequence")
	@Column(name = "ID")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "OS_ID")
	public OsList getOsId() {
		return osId;
	}

	public void setOsId(OsList osId) {
		this.osId = osId;
	}

	@Column(name = "SYMBOLIC_NAME")
	public String getSymbolicName() {
		return symbolicName;
	}

	public void setSymbolicName(String symbolicName) {
		this.symbolicName = symbolicName;
	}

	@Column(name = "STATUS")
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "OsDocument [id=" + id + ", osId=" + osId + ", symbolicName=" + symbolicName + ", status=" + status
				+ "]";
	}

}
