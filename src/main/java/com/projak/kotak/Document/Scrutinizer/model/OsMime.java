package com.projak.kotak.Document.Scrutinizer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "OS_MIME")
public class OsMime {
	
	private int id;
	
	private OsDocument osDocument;
	
	private String mimeType;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "mime_sequence")
	@SequenceGenerator(allocationSize = 1,name = "mime_sequence",sequenceName = "mime_sequence")
	@Column(name = "ID")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name = "DOC_ID")
	public OsDocument getOsDocument() {
		return osDocument;
	}

	public void setOsDocument(OsDocument osDocument) {
		this.osDocument = osDocument;
	}

	@Column(name = "MIMETYPE")
	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	@Override
	public String toString() {
		return "OsMime [id=" + id + ", osDocument=" + osDocument + ", mimeType=" + mimeType + "]";
	}
	
	

}
