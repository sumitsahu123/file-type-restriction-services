package com.projak.kotak.Document.Scrutinizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocumentScrutinizerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocumentScrutinizerApplication.class, args);
	}

}
