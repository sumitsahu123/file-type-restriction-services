package com.projak.kotak.Document.Scrutinizer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.projak.kotak.Document.Scrutinizer.model.OsDocument;
import com.projak.kotak.Document.Scrutinizer.model.OsMime;

public interface MimeRepository extends JpaRepository<OsMime, Integer> 
{

	OsMime findByMimeType(String mimeTypes);

	OsMime findByMimeTypeAndOsDocument(String mimeTypes, OsDocument osDocumentObj);

	@Query(value = "SELECT * FROM OS_MIME WHERE DOC_ID=?1 AND MIMETYPE=?2", nativeQuery = true)
	public OsMime checkMimeType(int docId, String mimeType);

}
