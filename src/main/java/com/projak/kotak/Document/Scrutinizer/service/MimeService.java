package com.projak.kotak.Document.Scrutinizer.service;

import org.json.JSONObject;

public interface MimeService {
	
	JSONObject create(JSONObject jobj) throws Exception;

	JSONObject delete(JSONObject jobj) throws Exception;

	boolean check(String OS, String document, String mime);

}
