package com.projak.kotak.Document.Scrutinizer.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
	
	@Autowired
	private Environment env;
	
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.ldapAuthentication()
		.userDnPatterns(env.getProperty("ldap.userDnPatterns"))
		.userSearchFilter(env.getProperty("ldap.userSearchFilter"))
		.groupSearchBase(env.getProperty("ldap.groupSearchBase"))
		.groupSearchFilter(env.getProperty("ldap.groupSearchFilter"))
		.contextSource()
		.url(env.getProperty("ldap.url"))
		.managerDn(env.getProperty("ldap.managerDn"))
		.managerPassword(env.getProperty("ldap.managerPassword"));
	}
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.httpBasic().disable().csrf().disable();
	}
	
	@Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
