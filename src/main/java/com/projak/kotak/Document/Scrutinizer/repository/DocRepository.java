package com.projak.kotak.Document.Scrutinizer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.projak.kotak.Document.Scrutinizer.model.OsDocument;
import com.projak.kotak.Document.Scrutinizer.model.OsList;

public interface DocRepository extends JpaRepository<OsDocument, Integer> {
	
	@Query(value = "SELECT * FROM OS_DOCUMENT WHERE SYMBOLIC_NAME=?1 AND OS_ID=?2",nativeQuery = true)
	public OsDocument getDocByName(String docName, int osId);

	OsDocument findBySymbolicName(String documentName);

	OsDocument findByOsId(OsList osList);

	OsDocument findBySymbolicNameAndOsId(String documentName, OsList osListObj);

}
