package com.projak.kotak.Document.Scrutinizer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.projak.kotak.Document.Scrutinizer.model.OsList;

public interface ListRepository extends JpaRepository<OsList, Integer> {
	
	@Query(value = "SELECT * FROM OS_LIST WHERE OBJECT_STORE=?1",nativeQuery = true)
	public OsList getOsByName(String osName);

}
